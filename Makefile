NAME = fractol

HEAD = includes/

VPATH = srcs:includes

FLAGS = -O3 -Wall -Wextra -Werror -I $(HEAD)

MLX = -lmlx -framework AppKit -framework OpenGl

SRCS = main.c									\
		picasso.c 								\
		events.c 								\
		elton.c 								\
		mandelbro.c								\
		julia.c 								\
		ship.c 									\
		wormhole.c 								\
		spider.c 								\
		serp.c 									\
		batoid.c								\
		onufriy.c 								\

BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
	 make re -C libft/ fclean && make -C libft/
	gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)

%.o: %.c
	gcc -I libft/includes $(FLAGS) -c -o $@ $<

clean:
	/bin/rm -f $(BINS) && make -C libft/ fclean

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
