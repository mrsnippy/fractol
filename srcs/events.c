/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 11:07:04 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 12:18:53 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_redraw(t_mlx *mlx)
{
	mlx_destroy_image(mlx->init, mlx->img);
	mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
	ft_restart(mlx);
	mlx_clear_window(mlx->init, mlx->win);
	mlx_put_image_to_window(mlx->init, mlx->win, mlx->img, 0, 0);
	ft_menu(mlx);
}

int		ft_zoom(int keycode, int x, int y, t_mlx *mlx)
{
	x -= WIN_W / 2;
	y -= WIN_H / 2;
	mlx->dx += x * mlx->zoom;
	mlx->dy += y * mlx->zoom;
	if (keycode == 4)
		mlx->zoom /= 1.25;
	else if (keycode == 5)
		mlx->zoom *= 1.25;
	mlx->dx -= x * mlx->zoom;
	mlx->dy -= y * mlx->zoom;
	mlx_destroy_image(mlx->init, mlx->img);
	mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
	ft_restart(mlx);
	mlx_clear_window(mlx->init, mlx->win);
	mlx_put_image_to_window(mlx->init, mlx->win, mlx->img, 0, 0);
	ft_menu(mlx);
	return (0);
}

int		ft_julia_party(int x, int y, t_mlx *mlx)
{
	static int flag;

	if (!flag)
		flag = 1;
	if (mlx->party_flag == 1 && x && y && mlx->fra == 0)
	{
		mlx_destroy_image(mlx->init, mlx->img);
		mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
		if (flag == 1)
		{
			mlx->jc += 0.03;
			flag = (mlx->jc > 0.4) ? 2 : flag;
		}
		if (flag == 2)
		{
			mlx->jc -= 0.03;
			flag = (mlx->jc < -0.7) ? 1 : flag;
		}
		ft_restart(mlx);
		mlx_clear_window(mlx->init, mlx->win);
		mlx_put_image_to_window(mlx->init, mlx->win, mlx->img, 0, 0);
		ft_menu(mlx);
	}
	return (1);
}

void	ft_events2(int keycode, t_mlx *mlx)
{
	if (keycode == 69)
	{
		mlx->iter++;
		ft_redraw(mlx);
	}
	if (keycode == 78 && mlx->iter >= 0)
	{
		mlx->iter--;
		ft_redraw(mlx);
	}
	if (keycode == 126 || keycode == 125 || keycode == 124 || keycode == 123)
	{
		if (keycode == 126)
			mlx->yst += 5;
		if (keycode == 125)
			mlx->yst -= 5;
		if (keycode == 124)
			mlx->xst -= 5;
		if (keycode == 123)
			mlx->xst += 5;
		ft_redraw(mlx);
	}
}

int		ft_events(int keycode, t_mlx *mlx)
{
	if (keycode == 53 && mlx)
		exit(0);
	if (keycode == 8 && mlx)
	{
		mlx->c += 50;
		ft_redraw(mlx);
	}
	if (keycode == 7)
	{
		mlx->party_flag = (mlx->party_flag == 0) ? 1 : 0;
		mlx_hook(mlx->win, 6, 1L << 6, ft_julia_party, mlx);
	}
	ft_events2(keycode, mlx);
	return (0);
}
