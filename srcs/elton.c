/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elton.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/24 08:52:29 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 11:17:40 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_pxl_img(int x, int y, int rgb, t_mlx *mlx)
{
	int				bpp;
	int				sl;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->img, &bpp, &sl, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (x > 0 && x < WIN_W && y > 0 && y < WIN_H)
	{
		ft_memcpy((void *)(image + sl * y + x * sl / WIN_W),
			(void *)&tmp, 4);
	}
}

void	ft_john(float **ds, float **err, int **x, int **y)
{
	(*ds)[0] = abs((*x)[1] - (*x)[0]);
	(*ds)[1] = abs((*y)[1] - (*y)[0]);
	(*ds)[2] = ((*x)[0] < (*x)[1]) ? 1 : -1;
	(*ds)[3] = ((*y)[0] < (*y)[1]) ? 1 : -1;
	(*err)[0] = (*ds)[0] - (*ds)[1];
}

void	ft_elton(int *x, int *y, int color, t_mlx *mlx)
{
	float	*ds;
	float	*err;

	err = (float *)malloc(sizeof(float) * 2);
	ds = (float *)malloc(sizeof(float) * 4);
	ft_john(&ds, &err, &x, &y);
	while (x[0] != x[1] || y[0] != y[1])
	{
		err[1] = err[0] * 2;
		if (err[1] > -ds[1])
		{
			err[0] -= ds[1];
			x[0] += ds[2];
		}
		if (err[1] < ds[0])
		{
			err[0] += ds[0];
			y[0] += ds[3];
		}
		ft_pxl_img(x[0], y[0], color, mlx);
	}
	free(err);
	free(ds);
}
