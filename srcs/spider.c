/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spider.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 12:23:20 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 19:19:12 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_sp_norma_dura(float **cxy, t_fra *fra, t_mlx *mlx)
{
	fra->n = 0;
	(*cxy)[0] = (fra->c.x) * 0.005 * mlx->zoom;
	(*cxy)[1] = (fra->c.y) * 0.005 * mlx->zoom;
	fra->z.x = 0;
	fra->z.y = 0;
	while (fra->z.x * fra->z.x + fra->z.y * fra->z.y < fra->max
		&& fra->n < fra->iter)
	{
		fra->t = fra->z;
		fra->z.x = (fra->t.x * fra->t.x) - (fra->t.y * fra->t.y)
			+ (*cxy)[0];
		fra->z.y = (2 * fra->t.x * fra->t.y) + (*cxy)[1];
		(*cxy)[0] = ((*cxy)[0] / 2) + fra->z.x;
		(*cxy)[1] = ((*cxy)[1] / 2) + fra->z.y;
		fra->n++;
	}
}

void		ft_spider(t_mlx *mlx)
{
	t_fra	fra;
	float	*cxy;

	ft_fra_init(&fra, mlx);
	cxy = (float *)malloc(sizeof(float) * 2);
	fra.c.y = -fra.my + mlx->dy / mlx->zoom + mlx->yst;
	while (fra.y <= WIN_H)
	{
		fra.c.x = -fra.mx + mlx->dx / mlx->zoom + mlx->xst;
		fra.x = 0;
		while (fra.x <= WIN_W)
		{
			ft_sp_norma_dura(&cxy, &fra, mlx);
			if (fra.n < fra.iter)
			{
				ft_pxl_img(fra.x, fra.y, (mlx->c - fra.n % mlx->c) * (mlx->c -
					fra.n % mlx->c) * (mlx->c - fra.n % mlx->c), mlx);
			}
			fra.x++;
			fra.c.x++;
		}
		fra.y++;
		fra.c.y++;
	}
	free(cxy);
}
