/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/26 13:09:18 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 13:13:03 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_mans_norma_dura(float **cxy, t_fra *fra, t_mlx *mlx)
{
	fra->n = 0;
	(*cxy)[0] = 0.6;
	(*cxy)[1] = 1.1;
	fra->z.x = (fra->c.x) * 0.005 * mlx->zoom;
	fra->z.y = (fra->c.y) * 0.005 * mlx->zoom;
	while (fra->z.x * fra->z.x + fra->z.y * fra->z.y < fra->max
		&& fra->n < fra->iter)
	{
		fra->t = fra->z;
		fra->z.x = sin(fra->t.x * (*cxy)[0]) -
			4 * (fra->t.y * (*cxy)[1]) - (*cxy[0]);
		fra->z.y = tan(fra->t.x * (*cxy)[1]) +
			cos(fra->t.y * (*cxy)[0]) - (*cxy)[1];
		fra->n++;
	}
}

void		ft_serpinsky(t_mlx *mlx)
{
	t_fra	fra;
	float	*cxy;

	ft_fra_init(&fra, mlx);
	cxy = (float *)malloc(sizeof(float) * 2);
	fra.c.y = -fra.my + mlx->dy / mlx->zoom + mlx->yst;
	while (fra.y <= WIN_H)
	{
		fra.c.x = -fra.mx + mlx->dx / mlx->zoom + mlx->yst;
		fra.x = 0;
		while (fra.x <= WIN_W)
		{
			ft_mans_norma_dura(&cxy, &fra, mlx);
			if (fra.n < fra.iter)
			{
				ft_pxl_img(fra.x, fra.y, (mlx->c - fra.n % mlx->c) * (mlx->c -
					fra.n % mlx->c) * (mlx->c - fra.n % mlx->c), mlx);
			}
			fra.x++;
			fra.c.x++;
		}
		fra.y++;
		fra.c.y++;
	}
	free(cxy);
}
