/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   picasso.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 14:02:38 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 12:16:49 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_menu(t_mlx *p)
{
	char	*tmp;
	char	*tmp2;

	tmp = ft_itoa(p->iter + 30);
	tmp2 = ft_strjoin("iterations: ", tmp);
	mlx_string_put(p->init, p->win, 10, 10, 16777215, tmp2);
	mlx_string_put(p->init, p->win, 10, 50, 16777215,
		"use arrow keys for displacement");
	mlx_string_put(p->init, p->win, 10, 70, 16777215,
		"+/- will change iterations amount");
	mlx_string_put(p->init, p->win, 10, 90, 16777215,
		"c will switch color palette");
	free(tmp);
	free(tmp2);
}

void	ft_my_mlx_init(t_mlx *mlx, int fra)
{
	mlx->fra = fra;
	mlx->init = mlx_init();
	mlx->win = mlx_new_window(mlx->init, WIN_W, WIN_H, "fract'ol");
	mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
	mlx->zoom = 1;
	mlx->dx = 0;
	mlx->dy = 0;
	mlx->c = 50;
	mlx->jc = 0;
	mlx->iter = 0;
	mlx->xst = 0;
	mlx->yst = 0;
}

void	ft_picasso(int fra, void (*func)(t_mlx *mlx))
{
	t_mlx	*mlx;

	mlx = (t_mlx *)malloc(sizeof(t_mlx));
	ft_my_mlx_init(mlx, fra);
	func(mlx);
	mlx_put_image_to_window(mlx->init, mlx->win, mlx->img, 0, 0);
	ft_menu(mlx);
	mlx_mouse_hook(mlx->win, ft_zoom, mlx);
	mlx_key_hook(mlx->win, ft_events, mlx);
	mlx_loop(mlx->init);
}
