/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   batoid.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/26 14:21:56 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/26 16:25:42 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_bt_norma_dura(float **cxy, t_fra *fra, t_mlx *mlx)
{
	fra->n = 0;
	(*cxy)[0] = (fra->c.x) * 0.005 * mlx->zoom;
	(*cxy)[1] = (fra->c.y) * 0.005 * mlx->zoom;
	fra->z.x = 0;
	fra->z.y = 0;
	while (fra->z.x * fra->z.x + fra->z.y * fra->z.y < fra->max
		&& fra->n < fra->iter)
	{
		fra->t = fra->z;
		fra->z.x = 2 * (fra->t.x * fra->t.x) - 2 * (fra->t.y * fra->t.y)
			+ (*cxy)[0];
		fra->z.y = (8 * fra->t.x * fra->t.y) + (*cxy)[1];
		(*cxy)[0] = ((*cxy)[0] / 2) + fra->z.x;
		(*cxy)[1] = ((*cxy)[1] / 2) + fra->z.y;
		fra->n++;
	}
}

void		ft_batoid(t_mlx *mlx)
{
	t_fra	fra;
	float	*cxy;

	ft_fra_init(&fra, mlx);
	cxy = (float *)malloc(sizeof(float) * 2);
	fra.c.y = -fra.my + mlx->dy / mlx->zoom + mlx->yst;
	while (fra.y <= WIN_H)
	{
		fra.c.x = -fra.mx + mlx->dx / mlx->zoom + mlx->xst;
		fra.x = 0;
		while (fra.x <= WIN_W)
		{
			ft_bt_norma_dura(&cxy, &fra, mlx);
			if (fra.n < fra.iter)
			{
				ft_pxl_img(fra.x, fra.y, (mlx->c - fra.n % mlx->c) * (mlx->c -
					fra.n % mlx->c) * (mlx->c - fra.n % mlx->c), mlx);
			}
			fra.x++;
			fra.c.x++;
		}
		fra.y++;
		fra.c.y++;
	}
	free(cxy);
}
