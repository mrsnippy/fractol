/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:08:04 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 12:16:26 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_print_options(char **str)
{
	int i;

	i = -1;
	ft_putstr("-----------------------------------\n");
	ft_putstr("Please use the following arguments:\n");
	while (++i < 8)
	{
		ft_putstr("  ---  ");
		ft_putstr(str[i]);
		ft_putendl("");
	}
	ft_putstr("-----------------------------------");
	exit(0);
}

int		ft_check(char *str)
{
	int		i;
	char	**fractals;

	i = -1;
	fractals = (char **)malloc(sizeof(char *) * 9);
	fractals[0] = "julia";
	fractals[1] = "mandelbro";
	fractals[2] = "ship";
	fractals[3] = "batoid";
	fractals[4] = "onufriy";
	fractals[5] = "spider";
	fractals[6] = "wormhole";
	fractals[7] = "mandelserp";
	fractals[8] = NULL;
	while (++i < 8)
	{
		if (ft_strcmp(str, fractals[i]) == 0)
			return (i);
	}
	ft_print_options(fractals);
	return (-1);
}

void	ft_start(int fra)
{
	if (fra == 0)
		ft_picasso(fra, ft_julia);
	if (fra == 1)
		ft_picasso(fra, ft_mandelbro);
	if (fra == 2)
		ft_picasso(fra, ft_ship);
	if (fra == 3)
		ft_picasso(fra, ft_batoid);
	if (fra == 4)
		ft_picasso(fra, ft_onufriy);
	if (fra == 5)
		ft_picasso(fra, ft_spider);
	if (fra == 6)
		ft_picasso(fra, ft_wormhole);
	if (fra == 7)
		ft_picasso(fra, ft_serpinsky);
}

void	ft_restart(t_mlx *mlx)
{
	if (mlx->fra == 0)
		ft_julia(mlx);
	if (mlx->fra == 1)
		ft_mandelbro(mlx);
	if (mlx->fra == 2)
		ft_ship(mlx);
	if (mlx->fra == 3)
		ft_batoid(mlx);
	if (mlx->fra == 4)
		ft_onufriy(mlx);
	if (mlx->fra == 5)
		ft_spider(mlx);
	if (mlx->fra == 6)
		ft_wormhole(mlx);
	if (mlx->fra == 7)
		ft_serpinsky(mlx);
}

int		main(int argc, char **argv)
{
	int		fra;

	if (argc == 2)
	{
		if ((fra = ft_check(argv[1])) >= 0)
			ft_start(fra);
	}
	return (0);
}
