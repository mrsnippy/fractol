/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 12:21:29 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/29 12:20:10 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <fcntl.h>
# include "libft.h"
# include <mlx.h>
# include <math.h>

# define WIN_W	1200
# define WIN_H	1200
# define COF	0.11

typedef struct	s_mlx
{
	void	*init;
	void	*win;
	void	*win2;
	void	*img;
	float	zoom;
	float	dx;
	float	dy;
	int		fra;
	int		c;
	float	jc;
	int		iter;
	int		party_flag;
	int		xst;
	int		yst;
}				t_mlx;

typedef struct	s_comp
{
	double	x;
	double	y;
}				t_comp;

typedef struct	s_fra
{
	int		x;
	int		y;
	int		n;
	int		mx;
	int		my;
	int		iter;
	int		max;
	t_comp	z;
	t_comp	c;
	t_comp	t;
}				t_fra;

void			ft_fra_init(t_fra *fra, t_mlx *mlx);
void			ft_picasso(int fra, void (*f)(t_mlx *mlx));
void			ft_elton(int *x, int *y, int color, t_mlx *mlx);
void			ft_pxl_img(int x, int y, int rgb, t_mlx *mlx);
void			ft_redraw(t_mlx *mlx);

void			ft_restart(t_mlx *mlx);
void			ft_menu(t_mlx *p);
int				ft_events(int keycode, t_mlx *mlx);
int				ft_zoom(int keycode, int x, int y, t_mlx *mlx);

void			ft_mandelbro(t_mlx *mlx);
void			ft_julia(t_mlx *mlx);

void			ft_ship(t_mlx *mlx);
void			ft_spider(t_mlx *mlx);
void			ft_wormhole(t_mlx *mlx);
void			ft_serpinsky(t_mlx *mlx);
void			ft_batoid(t_mlx *mlx);
void			ft_onufriy(t_mlx *mlx);

#endif
